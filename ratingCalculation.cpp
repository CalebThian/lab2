#include <iostream>
#include "ratingCalculation.h"
#include <cstdlib>
#include <cmath>
using namespace std;

Calculation::Calculation(float Rac,float Rbc, float Kc)
{
	Ra=Rac;
	Rb=Rbc;
	K=Kc;
};

//set function is deleted
void Calculation::SetRa(int Ras)
{
	Ra=Ras;
};

void Calculation::SetRb(int Rbs)
{
	Rb=Rbs;
};

void Calculation::SetK(int Ks)
{	
	K=Ks;
};


int Calculation::getRa()
{ 
	Ra=round(Ra);
	return Ra;
};

int Calculation::getRb()
{
	Rb= round(Rb);
	return Rb;
};

int Calculation::getK()
{
	return K;
};

float Calculation::getSa()
{
	return Sa;
};

float Calculation::calculationE()
{
	float P1=Rb/400-Ra/400;//power for calculate Ea
	float P2=-P1;//power for calculate Eb
	float D1=pow(10,P1)+1;//10 power of P1+1
	float D2=pow(10,P2)+1;//10 power of P2+1
	Ea=1/D1;//Ea calculation
	Eb=1/D2;//Eb calculation
	
};
	
float Calculation::calculationR()
{
	float difA=Sa-Ea;//Sa-Ea
	float difB=1-Sa-Eb;//Sb-Eb,note that Sa+Sb=1
	float adjA=K*difA;//K(Sa-Ea)
	float adjB=K*difB;//K(Sb-Eb)
	Ra=Ra+adjA;//calculation of Ra
	Rb=Rb+adjB;//calculation of Rb, Note that Sa+Sb=1
	//debug use
	//cout<<Ra<<" "<<Rb<<"\n";
	//cout<<difA<<" "<<difB<<" "<<adjA<<" "<<adjB<<" "<<Ea<<" "<<Eb<<" "<<"\n";
};

float Calculation::calculation()
{
	calculationE();
	calculationR();
};

void Calculation::printResult()
{
	cout<<getRa()<<" "<<getRb()<<"\n";
};

/*int Calculation::mainfunction()
{
	calculation();
	printResult();
};*/
