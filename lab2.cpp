#include <iostream>
#include <string>
#include <cstdlib>
#include "ratingCalculation.h"
using namespace std;

float getNum;//use to get data
float Rainp;//Ra get from input
float Rbinp;//Rb get from input
float Kinp;//maximum adjustment per game get from input
float scoreData;//score data include 0,0.5 and 1

void getInitialData()
{
	cin>>getNum;
	Kinp=getNum;
	cin>>getNum;
	Rainp=getNum;
	cin>>getNum;
	Rbinp=getNum;
	//debug use
	//cout<< Kinp<<" "<<Rainp<<" "<<Rbinp<<" ";

};

int cinLoop()
{
	Calculation Score(Rainp,Rbinp,Kinp);
	Score.printResult();
	while(cin>>scoreData)
	{
		//debug use;
	    //cout<<scoreData<<"\n";
		//Calculation Score(Rainp,Rbinp,Kinp);
		Score.Sa=scoreData;
		//debug use
		//cout<<Score.Sa<<"\n";
		//set function is deleted
		
		Score.SetRa(Rainp);
		Score.SetRb(Rbinp);
		Score.SetK(Kinp);
		
		Score.calculation();
		Score.printResult();
		Rainp=Score.getRa();
		Rbinp=Score.getRb();
	};
};

int main()
{
	getInitialData();
	cinLoop();
}


//all the code below is old code which use string data and atoi()


/*
string Null="";//delete a str
string str;//get score in str
string getstr;//get string
string Kstr;//maximum adjustment per game in str
string Rastr;//Rating of player a in str
string Rbstr;//Rating of player b in str


void getInitialData()
{
	cin>>getstr;
	Kstr=getstr;
	cin>>getstr;
	Rastr=getstr;
	getline(cin,getstr);
	Rbstr=getstr;
}	

int  getlineLoop()
{
	getline(cin,str);
	if(str != Null)
	{
	Calculation Score;
	Score.Sa=atoi(str.c_str());
	Score.setRa(Rastr);
	Score.setRb(Rbstr);
	Score.setK(Kstr);
	Score.mainfunction();
	str=Null;
	getlineLoop();
	}
	else
	{
		return 0;
	}
}

int main()
{
	getInitialData();
	cinLoop();
}
*/
