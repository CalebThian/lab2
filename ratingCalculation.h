#include <iostream>
#include <string>
using namespace std;

class Calculation{
	public:
		Calculation(float Rac,float Rbc,float Kc);
		float Sa;//score by player a
		//set function is deleted
		void SetRa(int Ras);//set Ra
		void SetRb(int Rbs);//set Rb
		void SetK(int Ks);//set K
		
		//old set function
		/*
		void setRa(string Rastring);//setRa from str
		void setRb(string Rbstring);//setRb from str
		void setK(string Kstring);//setK from str
		*/
		void printResult();//print out result
		int getRa();//return Ra
		int getRb();//return Rb
		int getK();//return K
		float getSa();//return Sa
		float calculation();//calculation
		int mainfunction();//main()
	private:
		float K;//maximum adjustment per game
		float Ra;//rating of player a
		float Rb;//rating of player b
		float Ea;//expected score of player a
		float Eb;//expected score of player b
		float calculationE();//calculation of expeceted score
		float calculationR();//calculation of rating
};
